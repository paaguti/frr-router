#!/bin/bash

# quagga 0.99.24.1 - arm64/amd64 packages
wget http://snapshot.debian.org/archive/debian/20150313T092905Z/pool/main/q/quagga/quagga_0.99.24.1-2_arm64.deb
wget http://snapshot.debian.org/archive/debian/20150313T092905Z/pool/main/q/quagga/quagga_0.99.24.1-2_amd64.deb

# quagga 0.99.23.1 - arm64/amd64 packages
wget http://snapshot.debian.org/archive/debian/20150313T092905Z/pool/main/q/quagga/quagga_0.99.23.1-1_arm64.deb
wget http://snapshot.debian.org/archive/debian/20150313T092905Z/pool/main/q/quagga/quagga_0.99.23.1-1_amd64.deb

# libreadline 6.8 - arm64/amd64 packages
wget https://plug-mirror.rcac.purdue.edu/ubuntu-ports/pool/main/r/readline6/libreadline6_6.3-8ubuntu2_arm64.deb
wget https://plug-mirror.rcac.purdue.edu/ubuntu/pool/main/r/readline6/libreadline6_6.3-8ubuntu2_amd64.deb
