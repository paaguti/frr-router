# Building UC3M's :es: Docker images for lightning :zap: - Router image :arrows_counterclockwise:

# Purpose
This README.md describes how to build a Docker image from a Dockerfile to be used within the [lightning network simulator](https://github.com/ptoribi/lightning).  
It also shows how to generate Docker images for other architectures different than the arquitecture of our computer by using [Docker Buildx](https://docs.docker.com/buildx/working-with-buildx/), a Docker plugin which employs [QEMU](https://www.qemu.org/) internally to emulate those arquitectures during the build phase.  

One image type is considered in this repository:
- [**Router**](https://hub.docker.com/repository/docker/ptoribi/lightning-uc3m-router): a Linux Docker container featuring the [Quagga Software Routing Suite](https://www.quagga.net/)

Two image architectures are considered for the image:
- **amd64**: also known as **x86_64**, CPU architecture widely used for servers, desktop and laptop computers and present in all modern Intel / AMD processors.
- **arm64**: also known as **aarch64** or **ARMv8**, CPU architecture famous for its power efficiency and commonly used in SBCs and microcontrollers. It is also the architecture used in the most recent Apple Silicon CPUs for computers (Apple M1/M2).


# Automated mode (CI/CD)
This repository has been configured to trigger a CI/CD pipeline after each commit, which will automatically build the Docker image and will upload it to the Docker registry, **tagging** the image with the same name as the **git branch** in which the commit was made.  
The recommended workflow is:
1. Create new branch from **latest**.
2. Make the necessary changes and commit+push to the new branch.
3. The pipeline will build the image and will upload it to the [Docker registry](https://hub.docker.com/repository/docker/ptoribi/lightning-uc3m-router) using as tag the name of the branch.
4. Test the new image in your computer with [lightning](https://github.com/ptoribi/lightning) by modifying the **DOCKER_IMAGE_router** variable inside [variables.conf](https://github.com/ptoribi/lightning/blob/master/variables.conf#L26).
5. If your test was successful and you want to promote this image to be the **latest** (optional), make a merge from the **new branch** into **latest**: the pipeline will build the image again and upload it to the Docker registry with the 'latest' tag.  


# Manual mode
The recommended workflow is:  
1. Build one image per CPU architecture desired and store it in the local Docker registry.
2. Test the new image in your computer with [lightning](https://github.com/ptoribi/lightning) by modifying the **DOCKER_IMAGE_router** variable inside [variables.conf](https://github.com/ptoribi/lightning/blob/master/variables.conf#L26).
3. If your test was successful now you can build the image again and upload it to Docker Hub.

## Requirements:
- Docker Engine - https://docs.docker.com/engine/install/
- Docker Buildx - https://docs.docker.com/buildx/working-with-buildx/
- Multi-arch support - https://docs.docker.com/desktop/multi-arch/

## Note on multi-arch images
- The local Docker registry (the one in your computer) only supports **single-arch** manifests, therefore when saved locally we must create one image per architecture (*-amd64, *-arm64).  
- On the contrary the Docker Hub registry supports **multi-arch** manifests and with only one build we can create a multi-arch image :sparkles:

## Local store registry (single-arch)
### Build image and store it locally (amd64/arm64):
```
docker buildx build --load --platform linux/amd64 -t lightning-uc3m-router-amd64 .
docker buildx build --load --platform linux/arm64 -t lightning-uc3m-router-arm64 .
```
### Test image stored locally:
```
docker run -it -d --rm --platform amd64 --name=test-lightning-uc3m-router-amd64 lightning-uc3m-router-amd64
docker exec -i -t --user=root test-lightning-uc3m-router-amd64 bash
docker container stop test-lightning-uc3m-router-amd64
```

## Docker Hub registry (multi-arch)
### Build image and store it in Docker Hub (amd64/arm64):
```
docker buildx build --push --platform linux/amd64,linux/arm64 -t ptoribi/lightning-uc3m-router:latest .
```
### Test image stored in Docker Hub:
```
docker run -it -d --rm --platform arm64 --name=test-lightning-uc3m-router ptoribi/lightning-uc3m-router
docker exec -i -t --user=root test-lightning-uc3m-router bash
docker container stop test-lightning-uc3m-router
```