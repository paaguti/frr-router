# Base image (Debian 9, "Stretch").
FROM debian:stretch

# As Debian Stretch reached its EOL, reset /etc/apt/sources.list to use the archive package repository
RUN echo "" > /etc/apt/sources.list && \
    echo "deb http://archive.debian.org/debian stretch main" >> /etc/apt/sources.list && \
    echo "deb http://archive.debian.org/debian-security stretch/updates main" >> /etc/apt/sources.list

# Update package indexes + upgrade all base packages + install new ones.
RUN apt-get update && apt-get upgrade -y && apt-get autoclean && apt-get autoremove
# Install new packages. Packages which create a service (ssh, xinetd, etc.) will try to start it afterwards, we can avoid it with RUNLEVEL=1.
RUN RUNLEVEL=1 apt-get install -y \
    iputils-ping \
    nano \
    net-tools \
    ssh \
    telnet \
    telnetd \
    traceroute \
    xinetd

# Install dependencies for quagga
# Version: 0.99.24.1-2
# Depends: libc6 (>= 2.15), libcap2 (>= 1:2.10), libpam0g (>= 0.99.7.1), libreadline6 (>= 6.0), libtinfo5, logrotate (>= 3.2-11), iproute2, debconf (>= 0.5) | debconf-2.0
RUN apt-get install -y \
    debconf \
    iproute2 \
    libc6 \
    libcap2 \
    libpam0g \
    libtinfo5 \
    logrotate	
# Dependency libreadline6 is not available in the official repos anymore, must be installed manually.
COPY software/quagga/libreadline6_6.3-8*.deb /tmp/
RUN dpkg -i /tmp/libreadline6_6.3-8*$(dpkg --print-architecture).deb

# Install quagga 0.99.23.1 (v0.99.23.1 not available in the official repos anymore, must be installed manually).
COPY software/quagga/quagga_0.99.23.1-1*.deb /tmp/
RUN dpkg -i /tmp/quagga_0.99.23.1-1*$(dpkg --print-architecture).deb

# Copy vtysh_persistent from host to container + add execution capabilities.
COPY software/scripts/vtysh_persistent /bin/
RUN chmod 755 /bin/vtysh_persistent

# CMD: command that will run when the container starts.
CMD ["bash"]
